-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 10 2014 г., 14:48
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `calls_clients`
--

-- --------------------------------------------------------

--
-- Структура таблицы `calls`
--

CREATE TABLE IF NOT EXISTS `calls` (
  `id_call` int(11) NOT NULL AUTO_INCREMENT,
  `city` text NOT NULL,
  `client_id` int(11) NOT NULL,
  `date_call` date NOT NULL,
  `time_call` time NOT NULL,
  `start` text NOT NULL,
  `finish` text NOT NULL,
  PRIMARY KEY (`id_call`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `calls`
--

INSERT INTO `calls` (`id_call`, `city`, `client_id`, `date_call`, `time_call`, `start`, `finish`) VALUES
(1, 'Львів', 1, '2013-04-16', '18:30:23', 'вул. Липинського 24\\43', 'вул. Городоцька 123\\23'),
(2, 'Київ', 2, '2012-02-03', '11:20:23', 'вул. Шевченка 42\\11', 'вул. Перемоги 52\\32'),
(3, 'Львів', 3, '2014-03-09', '15:45:23', 'вул. Мазепи 55\\11', 'вул. Личаківська 74\\24'),
(4, 'Львів', 1, '2014-01-09', '23:30:23', 'вул. Кульпарківська 58\\42', 'вул. Наукова 112\\24');

-- --------------------------------------------------------

--
-- Структура таблицы `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` text NOT NULL,
  `registration` date NOT NULL,
  `call_times` int(11) NOT NULL,
  `call_cancels` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `block_date` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `clients`
--

INSERT INTO `clients` (`id`, `phone`, `registration`, `call_times`, `call_cancels`, `status`, `block_date`) VALUES
(1, '0630456789', '2012-04-13', 15, 3, 1, ''),
(2, '0507896144', '2011-04-29', 37, 7, 1, ''),
(3, '0637878888', '2013-03-11', 9, 5, 0, '2014-03-11'),
(4, '0507884566', '2013-05-18', 45, 9, 1, ''),
(5, '0507884787', '2014-01-18', 5, 3, 0, '2014-02-13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
